public class Rectangle extends  Figure {

    public Rectangle(double r1, double r2, double x, double y) {
        super(r1, r2, x, y);
    }

    @Override
    public double getPerimeter() {
        return getX() * 2 + getY() * 2;
    }
}
