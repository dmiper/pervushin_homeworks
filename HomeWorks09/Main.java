public class Main {
    public static void main(String[] args) {
        Figure[] figures = new Figure[5];
        Figure figure = new Figure(10, 20, 15, 5);
        Rectangle rectangle = new Rectangle(4, 8, 12, 16);
        Ellipse ellipse = new Ellipse(5, 2, 6, 3);
        Square square = new Square(20, 10, 5, 15);
        Circle circle = new Circle(6, 3, 2, 5);

        figures[0] = figure;
        figures[1] = rectangle;
        figures[2] = ellipse;
        figures[3] = square;
        figures[4] = circle;

        for (Figure value : figures) {
            System.out.println("Периметр фигуры \t" + value.getPerimeter());
        }
    }
}
