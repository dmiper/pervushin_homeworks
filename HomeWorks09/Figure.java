public class Figure {

    private double r1;
    private double r2;
    private double x;
    private double y;

    public Figure(double r1, double r2, double x, double y) {
        this.r1 = r1;
        this.r2 = r2;
        this.x = x;
        this.y = y;
    }

    public double getR1() {
        return r1;
    }

    public double getR2() {
        return r2;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getPerimeter() {
        return 0;
    }
}
