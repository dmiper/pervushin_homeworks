package ru.pcs.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "account", schema = "schema", indexes = {
        @Index(name = "idx_account_id_account", columnList = "id_account")
}, uniqueConstraints = {
        @UniqueConstraint(name = "uc_account_id_account", columnNames = {"id_account"})
})
public class Account {
    public enum Role {
        ADMIN, USER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_account;
    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Column(unique = true)
    private String loginName;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String hashPassword;
    private String fullName;
}
