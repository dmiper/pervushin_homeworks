package ru.pcs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.models.Recordings;

public interface RecordingsRepository extends JpaRepository<Recordings, Integer> {
}
