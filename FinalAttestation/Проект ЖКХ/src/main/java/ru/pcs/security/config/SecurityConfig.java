package ru.pcs.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService accountDetailsServiceImpl;
    private final PasswordEncoder passwordEncoder;

    public SecurityConfig(UserDetailsService accountDetailsServiceImpl, PasswordEncoder passwordEncoder) {
        this.accountDetailsServiceImpl = accountDetailsServiceImpl;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/recordJkh").authenticated()
                .antMatchers("/recordJkh/**").hasAuthority("USER")
                .antMatchers("signUp").permitAll()
                .antMatchers("/recordJkh/**").authenticated()
                .and()
                .formLogin()
                .loginPage("/signIn")
                .usernameParameter("email")
                .passwordParameter("hashPassword")
                .defaultSuccessUrl("/recordJkh")
                .permitAll();
    }
}
