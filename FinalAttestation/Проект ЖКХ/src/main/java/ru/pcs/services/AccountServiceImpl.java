package ru.pcs.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.forms.AccountForm;
import ru.pcs.models.Account;
import ru.pcs.repositories.AccountRepository;

@RequiredArgsConstructor
@Component
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUpAccounts(AccountForm form) {
        Account account = Account.builder()
                .role(Account.Role.USER)
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getHashPassword()))
                .loginName(form.getLoginName())
                .fullName(form.getFullName())
                .build();
        accountRepository.save(account);
    }
}
