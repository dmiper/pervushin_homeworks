import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] ARRAY = new int[] {0, 3, 6, 9, 30, 39, 60, 69, 90, 99, 300, 360, 369};

        int[] output = Sequence.filter(ARRAY, value -> value % 2 == 0 && value != 0);
        System.out.println(Arrays.toString(output));

        int[] output1 = Sequence.filter(ARRAY, value -> sumOfDigits(value) % 2 == 0);
        System.out.println(Arrays.toString(output1));
    }

    private static int sumOfDigits(int i) {
        return i == 0 ? 0 : ((i % 10) + sumOfDigits(i / 10));
    }
}
