public class Square extends Rectangle implements Portable {

    public Square(double r1, double r2, double x, double y, String name) {
        super(r1, r2, x, y, name);
    }

    @Override
    public void moveShape(double newX, double newY) {
        setX(newX);
        setY(newY);
    }
}
