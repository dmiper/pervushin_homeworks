public abstract class Figure {

    private final double r1;
    private final double r2;
    private double x, y;
    private final String name;

    public Figure(double r1, double r2, double x, double y, String name) {
        this.r1 = r1;
        this.r2 = r2;
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public double getR1() {
        return r1;
    }

    public double getR2() {
        return r2;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getName() {
        return name;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public abstract double getPerimeter();
}
