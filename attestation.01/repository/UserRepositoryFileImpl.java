package attestation_01.repository;

import attestation_01.User;

import java.util.Map;

public class UserRepositoryFileImpl implements UserRepositoryFile{
    private final FileService fileService;
    private final String fileName;

    public UserRepositoryFileImpl(String fileName) {
        this.fileService = new FileServiceTXT();
        this.fileName = fileName;
    }

    @Override
    public User findByID(int searchID) throws NullPointerException {
        Map<Integer, User> users = fileService.inputFile(this.fileName);
        if (users.containsKey(searchID)) {
            return users.get(searchID);
        } else {
            System.err.println("ID не найден");
        }
        return null;
    }

    @Override
    public void update(User user) {
        Map<Integer, User> users = fileService.inputFile(this.fileName);
        users.put(user.getID(), user);
        fileService.updateUser(users, this.fileName);
    }
}
