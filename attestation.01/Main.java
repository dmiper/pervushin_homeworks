package attestation_01;

import attestation_01.repository.UserRepositoryFile;
import attestation_01.repository.UserRepositoryFileImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fileName = "attestations_users.txt";
        UserRepositoryFile userRepositoryFile = new UserRepositoryFileImpl(fileName);
        int searchID;
        User user;
        do {
            System.out.println("Напишите ID какого пользователя заменить:");
            searchID = scanner.nextInt();
            user = userRepositoryFile.findByID(searchID);
        } while (user == null);
        System.out.println("Старый " + user);
        System.out.println("Напишите ваше имя:");
        String name = scanner.next();
        user.setName(name);
        System.out.println("Напишите свой возраст:");
        int age = scanner.nextInt();
        user.setAge(age);
        System.out.println("Новый " + user);
        userRepositoryFile.update(user);
    }
}
