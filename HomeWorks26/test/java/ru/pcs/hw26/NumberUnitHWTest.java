package ru.pcs.hw26;

import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.pcs.vebinar26.NumbersUnit;


import static org.junit.jupiter.api.Assertions.*;

class NumberUnitHWTest {
    private final NumbersUnit numbersUnit = new NumbersUnit();
    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {
        @ParameterizedTest(name = "retern {2} on {0}, {1}")
        @CsvSource(value = {
                "15, 10, 5",
                "250, 100, 50",
                "60, 48, 12"})
        public void return_a_nice_answer(int a, int b, int result) {
            assertEquals(result, numbersUnit.gcd(a, b));
        }

        @ParameterizedTest(name = "throw exception on {0}, {1}")
        @CsvSource(value = {
                "-15, 10",
                "0, -100",
                "-60, -48"})
        public void bad_numbers_throw_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUnit.gcd(a, b));
        }
    }
}