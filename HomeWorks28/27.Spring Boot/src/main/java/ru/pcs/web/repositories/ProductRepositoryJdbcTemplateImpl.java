package ru.pcs.web.repositories;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Product;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductRepositoryJdbcTemplateImpl implements ProductsRepository {
    JdbcTemplate jdbcTemplate;
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";
    //language=SQL
    private static final String SQL_SELECT_SAVE = "insert into product(description, cost_rub, quantity) values(?, ?, ?)";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = ?";
    //language=SQL
    private static final String SQL_SELECT_UPDATE = "update product set description = ?, cost_rub = ?, quantity = ? where product.id = ?";

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNum) -> {
        Integer id = row.getInt("id");
        String description = row.getString("description");
        Integer cost_rub = row.getInt("cost_rub");
        Integer quantity = row.getInt("quantity");
        return new Product(id, description, cost_rub, quantity);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(@NotNull Product product) {
        jdbcTemplate.update(SQL_SELECT_SAVE, product.getDescription(), product.getCost_rub(), product.getQuantity());
    }

    @Override
    public void update(Product product, Integer productId) {
        jdbcTemplate.update(SQL_SELECT_UPDATE, product.getDescription(), product.getCost_rub(), product.getQuantity(),  productId);
    }

    @Override
    public void deleteById(Integer productId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, productId);
    }

    @Override
    public Product getById(Integer productId) {
        return null;
    }
}
