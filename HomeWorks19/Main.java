package HomeWorks;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        UserRepository userRepository = new UsersRepositoryFileImpl("users.txt");

        List<User> usersAgePeople = userRepository.findByAge(24);
        for (User user : usersAgePeople) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        List<User> usersByIsWorkerIsTrue = userRepository.findByIsWorkerIsTrue();
        for (User user : usersByIsWorkerIsTrue) {
            System.err.println(user.isWorker() + " " + user.getName() + " " + user.getAge());
        }
    }
}
