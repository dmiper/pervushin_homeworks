import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        String lowerCase = string.toLowerCase();
        String[] massive = lowerCase.split(" ");
        Map<String, Integer> map = new HashMap<>();
        int count = 1;
        for (String text : massive) {
            if (map.containsKey(text)) {
                map.put(text, map.get(text) + count);
            } else map.put(text, count);
        }
        map.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).forEach(System.out::println);
    }
}
