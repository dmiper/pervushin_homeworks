package hw21;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int[] array;
    public static int[] threadSums;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        int numbersCount = scanner.nextInt();
        System.out.println("Сколько будет потоков считать сумму чисел?");
        int threadsCount = scanner.nextInt();
        array = new int[numbersCount];
        threadSums = new int[threadsCount];
        fillArrayByRandoms(array);
        fillSumThreadArray(numbersCount, threadsCount);
        int realSum = getRealSum(array);
        int byThreadSum = getRealSum(threadSums);
        if (byThreadSum == realSum) {
            System.out.println("Суммы равны. Сумма массива равна вычислениям через потоки");
        } else {
            System.err.println("Проверьте свой код. Суммы не равны");
        }
    }

    static void fillArrayByRandoms(int[] array) {
        Random random = new Random();
        Arrays.setAll(array, i -> random.nextInt(100));
    }

    static int getRealSum(int[] array) {
        return Arrays.stream(array).sum();
    }

    static void fillSumThreadArray(int numbersCount, int threadsCount) {
        int from = 0;
        int to;
        int number = 0;
        int remainingThreadsCount = threadsCount;
        for (int i = 0; i < threadsCount; i++) {
            int numbersPerThread = getCountForThread(numbersCount, remainingThreadsCount);
            numbersCount -= numbersPerThread;
            remainingThreadsCount--;
            to = from + numbersPerThread;
            Thread thread = new SumThread(from, to, number);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
            from = to;
            number++;
        }
    }

    static int getCountForThread(int remainingNumbersCount, int remainingThreadsCount) {
        int numbersPerThread = remainingNumbersCount / remainingThreadsCount;
        if (remainingNumbersCount % remainingThreadsCount != 0) numbersPerThread++;
        return numbersPerThread;
    }
}

