import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Arrays.sort(peoples);
        printArr();
    }

    public static final People[] peoples = new People[] {
            new People("Vincent Willem Van Gogh", 63.7),
            new People("Joseph Vissarionovich Stalin", 92.2),
            new People("Jason Statham", 87.6),
            new People("Kim Jong-un", 125.8),
            new People("Pushkin Aleksandr Sergeevich", 78.3),
            new People("Michael Sylvester Gardenzio Stallone", 102.8),
            new People("Elizabeth II", 52.1),
            new People("Valentina Vladimirovna Tereshkova", 69.7),
            new People("Greta Tintin Eleonora Ernman Thunberg", 42.5),
            new People("Arianna Stassinopoulos Huffington", 51.3),
    };

    private static void printArr() {
        for (People value : Main.peoples) {
            System.out.println(value);
        }
    }
}
