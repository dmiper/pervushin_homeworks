package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {
    JdbcTemplate jdbcTemplate;

    private static final String SQL_SELECT_ALL = "select * from product order by id";
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from product where cost_rub > ?";

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNum) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        int cost_rub = row.getInt("cost_rub");
        int quantity = row.getInt("quantity");

        return new Product(id, description, cost_rub, quantity);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper, price);
    }
}
