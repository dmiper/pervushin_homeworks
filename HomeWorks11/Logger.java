public class Logger {
    private  static final Logger instance;

    static {
        instance = new Logger();
    }

    static Logger getInstance() {
        return instance;
    }

    private Logger() {}

    void log() {
        System.err.println("Сообщение из Журнала");
    }
}
